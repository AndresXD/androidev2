package tabian.com.fragements;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




/**
 * Created by User on 4/9/2017.
 */

public class Fragment1 extends Fragment {
    private static final String TAG = "Fragment1";

    private Button btnNavFrag1;
    private Button btnNavFrag2;
    private Button btnNavFrag3;
    private Button btnNavSecondActivity;
    public EditText name,edad;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {


        View view  = inflater.inflate(R.layout.fragment1_layout, container, false);
        btnNavFrag1 = (Button) view.findViewById(R.id.btnNavFrag1);
        btnNavFrag2 = (Button) view.findViewById(R.id.btnNavFrag2);
        btnNavFrag3 = (Button) view.findViewById(R.id.btnNavFrag3);
        btnNavSecondActivity = (Button) view.findViewById(R.id.btnNavSecondActivity);
        Log.d(TAG, "onCreateView: started.");

        btnNavFrag1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Su Contraseña es :", Toast.LENGTH_SHORT).show();




            }
        });




        btnNavFrag2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Fragment Datos de Contacto", Toast.LENGTH_SHORT).show();


              EditText names = (EditText)getActivity().findViewById(R.id.name);

                if ((names.getText().toString().trim().isEmpty())){



                    Toast.makeText(getActivity(), "Error faltan usuario  ", Toast.LENGTH_LONG).show();
                }else ((MainActivity) getActivity()).setViewPager(1);
            }
    });



                btnNavFrag3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getActivity(), "Fragment Datos de Inicio Sesion", Toast.LENGTH_SHORT).show();
                        ((MainActivity) getActivity()).setViewPager(2);
                    }
                });

                btnNavSecondActivity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });


        return view;
 }}